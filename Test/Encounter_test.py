import pytest
from Data import Encounter


encounter_name: str = "test_encounter"
encounter_max_life: int = 10
encounter_init_bonus: int = 10
encounter_img_path: str = "fake/path/img.jpg"
# encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)


def test_encounter_creation() -> None:
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)
	assert encounter.name == encounter_name
	assert encounter.maxLifePoint == encounter_max_life
	assert encounter.currentLifePoint == encounter_max_life
	assert encounter_img_path == encounter_img_path


def test_encounter_initiative() -> None:
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)
	assert encounter.initiativeDetails[0] >= 1
	assert encounter.initiativeDetails[0] <= 20
	assert encounter.initiativeDetails[1] == encounter_init_bonus
	assert encounter.initiativeDetails[1] == encounter.initiativeBonus
	assert encounter.initiativeBonus == encounter_init_bonus
	assert encounter.initiative == encounter.initiativeDetails[0] + encounter_init_bonus
	assert encounter.initiative == encounter.initiativeDetails[0] + encounter.initiativeDetails[1]


@pytest.mark.parametrize("init,bonus", [(0, 0), (5, 0), (0, 5), (5, 5)])
def test_encounter_initiative_setter(init: int, bonus: int) -> None:
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, bonus, encounter_img_path)
	encounter.SetInitiative(init, bonus)
	assert encounter.initiative == init + bonus


def test_encounter_reset():
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)
	encounter.Reset()
	assert encounter.maxLifePoint == encounter_max_life
	assert encounter.currentLifePoint == encounter_max_life
	test_encounter_initiative()
	encounter.Reset(True)
	assert encounter.initiativeBonus == 0


def test_encounter_death():
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)
	encounter.Reset()
	assert encounter.deathSave == 0
	assert encounter.isDead == False
	with pytest.raises(ValueError):
		encounter.DeathSave()
	encounter.currentLifePoint = 0
	encounter.DeathSave()
	assert encounter.deathSave != 0
	assert encounter.deathSave < 2
	assert encounter.deathSave > -2
	encounter.deathSave = -3
	assert encounter.isDead == True
