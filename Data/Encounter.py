"""Encounter"""
from Utils import Dice
from Logger import Logger, LogCategory, Color, coloredString


class Encounter:
	"""Encounter Class represent any creature in the combat"""

	def __init__(self, name: str, max_life_point: int, initiative_bonus: int = 0, image_path: str = ""):
		self.name: str = name
		self.imagePath: str = image_path
		self.currentLifePoint: int = max_life_point
		self.maxLifePoint: int = max_life_point
		self.deathSave: int = 0
		self._initiativeRoll: int = 0
		self._initiativeBonus: int = 0

		self.RollInitiative(bonus=initiative_bonus)

	def __str__(self):
		res: str = coloredString(f"{self.name}", Color.BOLD)
		if self.isDead:
			res += coloredString(" (dead)", Color.RED)
		else:
			res += coloredString(f" has ({self.currentLifePoint}/{self.maxLifePoint}) HP.", Color.GREEN)

		if self.initiative > 0:
			res += f" Initiative: {self.initiative} "
			if self._initiativeBonus > 0:
				res += f"({self._initiativeRoll}+{self._initiativeBonus})"
			else:
				res += f"({self._initiativeRoll})"

		return res

	def __repr__(self):
		return str(self)

	# region Initiative
	@property
	def initiativeDetails(self) -> tuple[int, int]:
		"""
		give the detail of initiative

		:return tuple[int, int] (rolled value, bonus)
		"""
		details: tuple[int, int] = (self._initiativeRoll, self._initiativeBonus)
		return details

	@property
	def initiativeBonus(self) -> int:
		"""initiative bonus Getter"""
		return self._initiativeBonus

	@property
	def initiative(self) -> int:
		"""return the rolled initiative"""
		return self._initiativeRoll + self._initiativeBonus

	def SetInitiative(self, value: int, bonus: int = 0) -> None:
		"""set initiative"""
		self._initiativeRoll = value
		self._initiativeBonus = bonus

	def RollInitiative(self, bonus: int = 0) -> int:
		"""roll a d20 and return the value + a bonus and set initiative"""
		self.SetInitiative(Dice.d20(), bonus)
		message: str = f"initiative roll for {self.name}: {coloredString(f'{self.initiative}({self.initiativeDetails[0]}+{self.initiativeDetails[1]})', Color.YELLOW)}"
		Logger.LogInfo(LogCategory.GAME, message)
		return self.initiative

	# endregion

	# region Death
	@property
	def isDead(self) -> bool:
		"""Check hp and death saves"""
		return self.currentLifePoint <= 0 and self.deathSave == -3

	def DeathSave(self) -> None:
		"""Throw a dice and update death status"""
		if self.currentLifePoint > 0:
			raise ValueError(coloredString(f"Can not make death save for {coloredString(f'{self.name}({self.currentLifePoint}/{self.maxLifePoint} hp)', Color.UNDERLINE)} to much life points.", Color.RED_BG))

		save_value: int = Dice.d20()
		if save_value <= 10:
			Logger.LogInfo(LogCategory.GAME_LOOP, coloredString("Death save failed", Color.RED))
			self.deathSave -= 1
		else:
			Logger.LogInfo(LogCategory.GAME_LOOP, coloredString("Death save success", Color.GREEN))
			self.deathSave += 1

		if self.deathSave == 3:
			Logger.LogInfo(LogCategory.GAME_LOOP, coloredString(f"{self.name} make 3 success against death back to 1hp", Color.GREEN_BG))
			self.deathSave = 0
			self.currentLifePoint = 1

	def printLifeStatus(self) -> None:
		"""show in console living status"""
		saves: str = str(self.deathSave) + " success" if self.deathSave >= 0 else str(self.deathSave * -1) + " fail"
		message = f"{self.name} has {coloredString(f'{self.currentLifePoint}/{self.maxLifePoint}', Color.BOLD)} hp and make {coloredString(saves, Color.BOLD)}"
		Logger.LogInfo(LogCategory.GAME, message)

	# endregion

	def Reset(self, reset_initiative_bonus: bool = False) -> None:
		"""set base value

		:param reset_initiative_bonus if False keep current bonus else set 0
		"""
		self.currentLifePoint = self.maxLifePoint
		self.deathSave = 0
		self.SetInitiative(1, 0 if reset_initiative_bonus else self._initiativeBonus)
		Logger.LogInfo(LogCategory.GAME, coloredString(f"reset {self.name}: hp restored, initiative rerolled", Color.YELLOW))
