"""Game"""
from Data import Encounter


class Game:
	"""Game class contain all information about a fight"""
	def __init__(self, name: str):
		self.name: str = name
		self._turn: int = 0

		self.encounters: list[Encounter] = []

	# region Getter / Setter
	@property
	def turn(self):
		"""return actual turn number"""
		return self._turn

	# endregion

	def NextTurn(self):
		"""Update turn number and change encounters turn base thing """
