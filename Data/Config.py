"""Configuration file"""
from dataclasses import dataclass, field
from configparser import ConfigParser
from Logger import Logger, LogLevel, LogCategory


@dataclass(repr=True)
class Config:
	"""Static class containing the program settings"""
	logLevel: LogLevel = field(repr=True, default=LogLevel.WARNING)
	hideLog: list[LogCategory] = field(repr=True, default_factory=list)
	savedEncountersLocation: str = field(repr=True, default="./SavedData/Encounters/")
	savedGamesLocation: str = field(repr=True, default="./SavedData/Games/")


def InitConfig() -> Config:
	"""Load config file and update project settings"""
	config: Config = Config()
	parser: ConfigParser = ConfigParser()
	with open("User.cfg", encoding="utf_8") as config_file:
		parser.read_file(config_file)

	if 'LOG' in parser:
		log_config = parser["LOG"]
		config.logLevel = LogLevel[log_config.get('loglevel', 'WARNING')]
		Logger.logLevelLimit = config.logLevel

		for category in log_config.get('hideLog', "DEBUG").strip("[]").split(","):
			if category != "":
				config.hideLog.append(LogCategory[category])
		Logger.hideLogsCategory = config.hideLog

	if 'STORAGE' in parser:
		log_config = parser["STORAGE"]
		config.savedEncountersLocation = log_config.get('savedEncountersLocation', './SavedData/Encounters/')
		config.savedGamesLocation = log_config.get('savedGamesLocation', './SavedData/Games/')

	return config
