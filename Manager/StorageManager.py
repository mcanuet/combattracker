"""Storage Manager"""
import os
import pickle
from typing import Optional

from Data import Encounter, Config
import Logger


class StorageManger:
	"""This manager handle the data saving and loading management"""
	_pickleExtension: str = ".pkl"

	# region Encounter
	@staticmethod
	def StoreEncounter(encounter: Encounter, rewrite: bool = False) -> None:
		"""Create a Pickle containing the information to quickly create an encounter

		:param encounter: The encounter to store, the name of the encounter will be used for the file name
		:param rewrite: if True it will rewrite the file who have the same name
		"""
		path: str = Config.savedEncountersLocation + encounter.name + StorageManger._pickleExtension
		if os.path.exists(path) and not rewrite:
			Logger.Logger.LogWarning(
				Logger.LogCategory.DATA_SAVE,
				f"A file already exist with the same name and rewrite mode is false path: {path}"
			)
			return

		with open(path, 'wb') as encounterFile:
			pickle.dump(encounter, encounterFile, protocol=pickle.DEFAULT_PROTOCOL)

		Logger.Logger.LogTest(Logger.LogCategory.DATA_SAVE, f"File save successfully path: {path}")

	@staticmethod
	def LoadEncounter(name: str, reset_at_load: bool = False) -> Optional[Encounter]:
		"""Load and unpickle an encounter by using its name

		:param reset_at_load:
		:param name: the name of the encounter to load
		:return: None if there is no file for the given encounter
		"""
		path: str = Config.savedEncountersLocation + name + StorageManger._pickleExtension
		if not os.path.exists(path):
			Logger.Logger.LogError(Logger.LogCategory.DATA_LOAD,f"No file found for the specified encounter path: {path}")
			return None

		with open(path, 'rb') as encounterFile:
			encounter: Encounter = pickle.load(encounterFile)
		if reset_at_load:
			encounter.Reset()

		Logger.Logger.LogTest(Logger.LogCategory.DATA_LOAD, f"File Load successfully (reset: {reset_at_load}) path: {path}")

		return encounter
	# endregion
