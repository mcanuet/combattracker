"""Logger module"""
from .Core import Logger
from .Enums import LogLevel, LogCategory, Color
from .CPrint import coloredString, cprint
