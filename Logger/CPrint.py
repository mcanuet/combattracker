"""Console color"""
import os
from .Enums import Color
# necessary to make windows capable to use unix color tag


def coloredString(message: str, color: Color) -> str:
	"""return message enclosed by color tag

	:param message: string who is going to be print
	:param color: Color who colorize the message
	:return string: message enclosed by color tag
	"""
	return f"{color}{message}{Color.RESET}"


def cprint(message: str, color: Color, end: str = "") -> None:
	"""message entirely colored by color

	:param message: string who is going to be print
	:param color: Color who colorize the message
	:param end: string appended after the last value, default a newline.
	"""
	system = os.environ['OS']
	os.system("")
	print(coloredString(message, color), end=end)
	os.system(system)
