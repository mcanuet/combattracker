"""Logger enumerators"""
from enum import IntEnum


class LogLevel(IntEnum):
	"""Define the level of log"""
	DEBUG = 0
	INFO = 5
	TEST = 10
	WARNING = 20
	ERROR = 30
	CRITICAL = 40


class LogCategory(IntEnum):
	"""Define the log category"""
	DEBUG = 10
	SCRIPT = 100
	SCRIPT_INIT = 105
	SCRIPT_DATA = 110
	SCRIPT_UTILS = 120
	UI = 300
	UI_FRAME = 310
	UI_RESET = 320
	GAME = 400
	GAME_LOOP = 410
	GAME_SAVE = 420
	GAME_LOAD = 425
	DATA = 500
	DATA_SAVE = 510
	DATA_LOAD = 520


# pylint: disable=R0903
class Color:
	"""Allowed color and style in console"""
	# foreground color
	GREY = '\33[90m'
	RED = '\33[91m'
	GREEN = '\33[92m'
	YELLOW = '\33[93m'
	BLUE = '\33[94m'
	VIOLET = '\33[95m'
	BEIGE = '\33[96m'
	WHITE = '\33[97m'
	# background color
	GREY_BG = '\33[100m'
	RED_BG = '\33[101m'
	GREEN_BG = '\33[102m'
	YELLOW_BG = '\33[103m'
	BLUE_BG = '\33[104m'
	VIOLET_BG = '\33[105m'
	BEIGE_BG = '\33[106m'
	WHITE_BG = '\33[107m'
	# text aspect
	SELECTED = '\33[7m'
	UNDERLINE = '\033[4m'
	BOLD = '\33[1m'
	ITALIC = '\33[3m'
	BLINK = '\33[5m'
	RESET = '\033[0m'

	__slots__ = ()
# pylint: enable=R0903
