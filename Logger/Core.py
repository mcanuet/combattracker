"""Logger core script"""
import inspect
from .Enums import LogLevel, LogCategory, Color
from .CPrint import coloredString


class Logger:
	"""Logger """
	_logLevelAsColor = {
		LogLevel.DEBUG:		Color.BOLD,
		LogLevel.INFO:		Color.WHITE,
		LogLevel.TEST:		Color.GREEN,
		LogLevel.WARNING:	Color.YELLOW,
		LogLevel.ERROR:		Color.RED,
		LogLevel.CRITICAL:	Color.RED_BG
	}

	logLevelLimit: LogLevel = LogLevel.WARNING
	hideLogsCategory: list[LogCategory] = []

	__slots__ = ()

	# pylint: disable=R0903,R0913
	@staticmethod
	def Log(category: LogCategory, log_level: LogLevel, message: str, end="\n") -> None:
		"""Log the text in the console if the log level permit it

		:param category: the log category
		:param log_level: a level for the log
		:param message: the message to print
		:param end: the end of the printed string
		"""

		caller = inspect.getouterframes(inspect.currentframe(), 1)[2]
		if log_level < Logger.logLevelLimit or category in Logger.hideLogsCategory:
			return
		path_split = "\\"
		print(f"({coloredString(category.name,Color.BLUE)}:{coloredString(log_level.name, Logger._logLevelAsColor[log_level])}) {coloredString(f'[{caller.filename.split(path_split)[-1]}:{caller.function}: ln {caller.lineno}]', Logger._logLevelAsColor[log_level])} -> {message}", end=end)

	# pylint: enable=R0903,R0913

	@staticmethod
	def LogDebug(category: LogCategory, message: str, end="\n") -> None:
		"""Log a debug message"""
		Logger.Log(category, LogLevel.DEBUG, message, end)

	@staticmethod
	def LogInfo(category: LogCategory, class_name: str, end="\n") -> None:
		"""Log an info message"""
		Logger.Log(category, LogLevel.INFO, class_name, end)

	@staticmethod
	def LogTest(category: LogCategory, class_name: str, end="\n") -> None:
		"""Log a test message / result"""
		Logger.Log(category, LogLevel.TEST, class_name, end)

	@staticmethod
	def LogWarning(category: LogCategory, class_name: str, end="\n") -> None:
		"""Log a warning message"""
		Logger.Log(category, LogLevel.WARNING, class_name, end)

	@staticmethod
	def LogError(category: LogCategory, class_name: str, end="\n") -> None:
		"""Log an error message"""
		Logger.Log(category, LogLevel.ERROR, class_name, end)

	@staticmethod
	def LogCritical(category: LogCategory, class_name: str, end="\n") -> None:
		"""Log a critical message"""
		Logger.Log(category, LogLevel.CRITICAL, class_name, end)
