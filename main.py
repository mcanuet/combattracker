from sys import argv
from getopt import getopt, GetoptError
from Data import Encounter, Game, Config, InitConfig
from Logger import Logger, LogCategory
from Manager import StorageManger


def ResolveArgs() -> None:
	try:
		opts, args = getopt(argv, shortopts="", longopts=["logLevel="])
		Logger.LogDebug(LogCategory.SCRIPT_INIT, str((opts, args)))
	except GetoptError:
		Logger.LogDebug(LogCategory.SCRIPT_INIT, "An error occurred when trying to resolved arguments")
		return


class CombatTracker:
	def __init__(self):
		self.game: Game

	__slots__ = ("config", "game")


if __name__ == '__main__':
	InitConfig()
	ResolveArgs()

	combatTracker: CombatTracker = CombatTracker()
	combatTracker.game = Game("game")

	encounter_name: str = "test_encounter"
	encounter_max_life: int = 10
	encounter_init_bonus: int = 10
	encounter_img_path: str = "fake/path/img.jpg"
	encounter: Encounter = Encounter(encounter_name, encounter_max_life, encounter_init_bonus, encounter_img_path)

	print("______ TEST ______")
	Logger.LogDebug(LogCategory.SCRIPT, str(encounter))
	StorageManger.StoreEncounter(encounter, True)
	encounter2: Encounter = StorageManger.LoadEncounter(encounter_name, True)
	Logger.LogDebug(LogCategory.SCRIPT, str(encounter2))

