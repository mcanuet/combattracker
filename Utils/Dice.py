"""Dice"""
import random


# pylint: disable=C0116
class Dice:
	"""Dice use to quickly add some random number"""

	@staticmethod
	def d4() -> int:
		return random.randint(1, 4)

	@staticmethod
	def d6() -> int:
		return random.randint(1, 6)

	@staticmethod
	def d8() -> int:
		return random.randint(1, 8)

	@staticmethod
	def d10() -> int:
		return random.randint(1, 10)

	@staticmethod
	def d12() -> int:
		return random.randint(1, 12)

	@staticmethod
	def d20() -> int:
		return random.randint(1, 20)

	@staticmethod
	def d100() -> int:
		return random.randint(1, 100)

	@staticmethod
	def Custom(dice_size: int) -> int:
		return random.randint(1, dice_size)
# pylint: enable=C0116
